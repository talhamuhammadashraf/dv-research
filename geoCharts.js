google.charts.load("current", {
  packages: ["geochart"],
  // Note: you will need to get a mapsApiKey for your project.
  // See: https://developers.google.com/chart/interactive/docs/basic_load_libs#load-settings
  mapsApiKey: "AIzaSyD-9tSrke72PouQMnMX-a7eZSW0jkFMBWY",
});
google.charts.setOnLoadCallback(drawRegionsMap);

function drawRegionsMap() {
  var data = google.visualization.arrayToDataTable([
    ["Country", "Popularity"],
    ["Germany", 200],
    ["United States", 300],
    ["Brazil", 400],
    ["Canada", 500],
    ["France", 600],
    ["RU", 700],
  ]);

  var options = {};

  var chart = new google.visualization.GeoChart(
    document.getElementById("regions_div")
  );

  chart.draw(data, options);
}

const data = [
  { name: "<5", value: 19912018 },
  { name: "5-9", value: 20501982 },
  { name: "10-14", value: 20679786 },
  { name: "15-19", value: 21354481 },
  { name: "20-24", value: 22604232 },
  { name: "25-29", value: 21698010 },
  { name: "30-34", value: 21183639 },
  { name: "35-39", value: 19855782 },
  { name: "40-44", value: 20796128 },
  { name: "45-49", value: 21370368 },
  { name: "50-54", value: 22525490 },
  { name: "55-59", value: 21001947 },
  { name: "60-64", value: 18415681 },
  { name: "65-69", value: 14547446 },
  { name: "70-74", value: 10587721 },
  { name: "75-79", value: 7730129 },
  { name: "80-84", value: 5811429 },
  { name: "≥85", value: 5938752 },
];
console.log(data);
// d3.csv("cities.csv",(error, data) => {dataViz(data)});

// function dataViz(incomingData) {
// const incomingData = [
//   { population: 0 },
//   { population: 10 },
//   { population: 20 },
//   { population: 30 },
//   { population: 40 },
//   { population: 50 },
//   { population: 60 },
//   { population: 70 },
//   { population: 80 },
//   { population: 90 },
// ];
// var maxPopulation = d3.max(incomingData, (d) => parseInt(d.population));

// var yScale = d3.scaleLinear().domain([0, maxPopulation]).range([0, 90]);

// d3.select("svg").attr("style", "height: 480px; width:600px;");

// d3.select("svg")

//   .selectAll("rect")

//   .data(incomingData)

//   .enter()

//   .append("rect")

//   .attr("width", 50)

//   .attr("height", (d) => yScale(parseInt(d.population)))

//   .attr("x", (d, i) => i * 60)

//   .attr("y", (d) => 480 - yScale(parseInt(d.population)))

//   .style("fill", "#FE9922")

//   .style("stroke", "#9A8B7A")

//   .style("stroke-width", "1px");

// }
