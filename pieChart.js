google.charts.load("current", { packages: ["corechart"] });
google.charts.setOnLoadCallback(drawGoogleChartPie);

function drawGoogleChartPie() {
  // Create the data table.
  var data = new google.visualization.DataTable();
  data.addColumn("string", "Topping");
  data.addColumn("number", "Slices");
  data.addRows([
    ["Mushrooms", 8],
    ["Onions", 21],
    ["Olives", 15],
    ["Zucchini", 29],
    ["Pepperoni", 11],
    // ["Black Olives", 6],
  ]);
  console.log(data);
  // Set chart options
  var options = {
    title: "How Much Pizza I Ate Last Night",
    width: 400,
    height: 300,
  };

  // Instantiate and draw our chart, passing in some options.
  var chart = new google.visualization.PieChart(
    document.getElementById("pie_google")
  );
  chart.draw(data, options);
}
